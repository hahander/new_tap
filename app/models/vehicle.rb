class Vehicle < ActiveRecord::Base
  attr_accessible :level, :member_id, :name, :image, :blocked_to
  belongs_to :member
end
