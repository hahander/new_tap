class Member < ActiveRecord::Base
  include Enumerize
  attr_accessible :name, :rank, :account_id, :last_update, :battalion
  has_many :vehicles, dependent: :destroy

  enumerize :battalion, :in => %w[0 1 2 3], default: '0'
end
