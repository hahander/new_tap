class Ability
  include CanCan::Ability

  def initialize(user)
    @user = user
    if @user
      send(@user.role.presence)
    else
      send('guest')
    end
  end

  def admin
    can :manage, User
    can :manage, Page
    can :manage, Member
    can :manage, Vehicle
    can :access, :rails_admin
    can :dashboard
  end

  def moderator
    can :access, :rails_admin
    can :dashboard
    can :manage, Member
    can :manage, Vehicle
  end

  def user
    can :read, :all
  end

  def guest
    can :read, Member
  end
end
