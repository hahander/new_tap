module MembersHelper
  def last_update(date)
    if date < Date.today - 7
      content_tag('td', date.strftime('%d-%m-%Y %H-%M'), class: 'bad')
    else
      content_tag('td', date.strftime('%d-%m-%Y %H-%M'), class: 'good')
    end
  end
end
