module VehiclesHelper
  def get_image(name)
    image_tag "http://worldoftanks.ru/#{Vehicle.where(name: name).first.image}"
  end

  def get_count(name)
    Vehicle.where(name: name).count
  end
end
