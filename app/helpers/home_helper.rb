module HomeHelper
  def sign_in_link
    if current_user.nil?
      link_to 'Sign In', new_user_session_path
    else
      link_to 'Sign Out', destroy_user_session_path, method: :delete
    end
  end
end
