class MembersController < ApplicationController
  load_and_authorize_resource

  respond_to :js, only: :show

  def index
    @members = @members.order("LOWER(name)")
  end

  def get_all
    authorize! :manage, Member
    url = 'http://api.worldoftanks.ru/community/clans/988/api/1.1/?source_token=WG-WoT_Assistant-1.3.2'
    clan_members = JSON.parse(open(url).read)["data"]["members"]
    clan_members.each do |member|
      if Member.where(name: member["account_name"]).empty?
        @member = Member.new(name: member["account_name"], rank: member["role_localised"], account_id: member["account_id"])
        @member.save

      end
    end
    Member.all.each do |member|
      if clan_members.select{|arr| arr["account_name"] == "#{member.name}"}.empty?
        member.destroy
      end
    end
    flash[:notice] = 'Members list updated'
    redirect_to members_path
  end

  def first_battalion
    @members = Member.where(battalion: 1).order("LOWER(name)")
    members_ids = @members.map(&:id)
    @vehicles = Vehicle.where('member_id IN (?)', members_ids).where(level: '10')
  end

  def second_battalion
    @members = Member.where(battalion: 2).order("LOWER(name)")
    members_ids = @members.map(&:id)
    @vehicles = Vehicle.where('member_id IN (?)', members_ids).where(level: '10')
  end

  def third_battalion
    @members = Member.where(battalion: 3).order("LOWER(name)")
    members_ids = @members.map(&:id)
    @vehicles = Vehicle.where('member_id IN (?)', members_ids).where(level: '10')
  end
end
