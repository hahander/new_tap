class VehiclesController < ApplicationController

  respond_to :js

  def get_all
    authorize! :manage, Vehicle
    url = "http://api.worldoftanks.ru/community/accounts/#{params[:id]}/api/1.9/?source_token=WG-WoT_Assistant-1.3.2"
    vehicles = JSON.parse(open(url).read)["data"]["vehicles"]
    vehicles.each do |vehicle|
      if Member.find(params[:member_id]).vehicles.where(name: vehicle["name"]).empty?
        @record = Vehicle.create(name: vehicle["name"], level: vehicle["level"], image: vehicle["image_url"], member_id: params[:member_id])
        if @record.save
          Member.find(params[:member_id]).update_attribute(:last_update, Time.now)
        end
      end
    end
    flash[:notice] = "Vehicles of member #{Member.find(params[:member_id]).name} accepted"
    redirect_to members_path
  end

  def show_owners
    authorize! :manage, Vehicle
    @vehicles = Vehicle.where(name: params[:vehicle])
  end

  def show_battalion_owners
    authorize! :manage, Vehicle
    @members = Member.where(battalion: params[:battalion])
    members_ids = @members.map(&:id)
    @vehicles = Vehicle.where('member_id IN (?)', members_ids).where(level: '10').where(name: params[:vehicle_name])
  end

  def check_block_status
    authorize! :manage, Vehicle
    Vehicle.where('vehicles.blocked_to <(?)', Time.now).update_all(blocked_to: nil)
    flash[:notice] = "Blocked status of vehicles updated"
    redirect_to members_path
  end
end
