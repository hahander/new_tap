class ApplicationController < ActionController::Base
  protect_from_forgery
  require 'open-uri'

  respond_to :html

  rescue_from CanCan::AccessDenied do |exception|
    redirect_to '/', :alert => exception.message
  end
end
