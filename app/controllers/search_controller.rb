class SearchController < ApplicationController
  def search_user

  end

  def search_clan

  end

  def user
    @json_object = get_user(params[:search][:user_name], 99)
  end

  def clan
    @json_object = get_clan(params[:search][:clan_name], 99)
  end

  def clan_description
    clan_url = "http://api.worldoftanks.ru/community/clans/#{params[:clan_id]}/api/1.1/?source_token=WG-WoT_Assistant-1.3.2"
    @clan_json = JSON.parse(open(clan_url).read)
    @vehicles = []
    @clan_json['data']['members'].each_with_index do |member, index|
      member_stat = get_user_stat(member['account_id'])
      @clan_json['data']['members'][index].merge!(member_stat["data"]) unless member_stat["status"] == 'error'
      @vehicles += member_stat['data']["vehicles"].select{|arr| arr['level'] == 10} unless member_stat["status"] == 'error'
    end
  end

  private
    def get_user(user_name, limit)
      url = "http://api.worldoftanks.ru/community/accounts/api/1.1/?source_token=WG-WoT_Assistant-1.3.2&search=#{user_name}&offset=0&limit=#{limit}"
      JSON.parse(open(url).read)
    end

    def get_clan(clan_name, limit)
      url = "http://api.worldoftanks.ru/community/clans/api/1.1/?source_token=WG-WoT_Assistant-1.3.2&search=#{clan_name}&offset=0&limit=#{limit}&order_by=name"
      JSON.parse(open(url).read)
    end

    def get_user_stat(user_id)
      url = "http://api.worldoftanks.ru/community/accounts/#{user_id}/api/1.9/?source_token=WG-WoT_Assistant-1.3.2"
      JSON.parse(open(url).read)
    end
end
