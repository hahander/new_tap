# Place all the behaviors and hooks related to the matching controller here.
# All this logic will automatically be available in application.js.
# You can use CoffeeScript in this file: http://jashkenas.github.com/coffee-script/

$ ->
  $(".member_details").click ->
    member_id = $(@).data("member-id")
    $('.popover').remove()
    $(@).popover(
      html: true
      placement: get_popover_placement
      title: "title"
      trigger: "manual"
    )
    $(@).popover "show"
    position = $(this).position()
    $('.popover').css("top", position.top - 150)
    $('.popover').css('min-width', 650)
    $('.popover').css('min-height', 200)
    pop_pos = $('.popover').position()
    temp = position.top - pop_pos.top
    proc = temp * 100 / 522
    y_coord = proc / 100 * 522 + 8
    $('.arrow').css("top", y_coord)
    $('.popover').hide()
    $.ajax(
      url: "/members/#{member_id}"
      cache: false
      ).done ->
        $('.popover').show()
        $(".close_popover").click ->
          $('.popover').remove()

  $("span.vehicle_details").click ->
    vehicle_name = $(@).data("vehicle-name")
    $('.popover').remove()
    $(@).popover(
      html: true
      placement: get_popover_placement
      title: "title"
      trigger: "manual"
    )
    $(@).popover "show"
    position = $(this).position()
    $('.popover').css("top", position.top - 150)
    $('.popover').css('min-width', 550)
    $('.popover').css('left', position.left - 555)
    $('.popover').css('min-height', 200)
    pop_pos = $('.popover').position()
    temp = position.top - pop_pos.top
    proc = temp * 100 / 522
    y_coord = proc / 100 * 522 + 8
    $('.arrow').css("top", y_coord)
    $('.popover').hide()
    $.ajax(
      url: "/show_owners/#{vehicle_name}"
      cache: false
      ).done ->
        $('.popover').show()
        $(".close_popover").click ->
          $('.popover').remove()

  $("span.battalion_vehicle_details").click ->
    vehicle_name = $(@).data("vehicle-name")
    battalion = $(@).data('battalion')
    $('.popover').remove()
    $(@).popover(
      html: true
      placement: get_popover_placement
      title: "title"
      trigger: "manual"
    )
    $(@).popover "show"
    position = $(this).position()
    $('.popover').css("top", position.top - 150)
    $('.popover').css('min-width', 550)
    $('.popover').css('left', position.left - 555)
    $('.popover').css('min-height', 200)
    pop_pos = $('.popover').position()
    temp = position.top - pop_pos.top
    proc = temp * 100 / 522
    y_coord = proc / 100 * 522 + 8
    $('.arrow').css("top", y_coord)
    $('.popover').hide()
    $.ajax(
      url: "/show_battalion_owners/#{battalion}/#{vehicle_name}"
      cache: false
      ).done ->
        $('.popover').show()
        $(".close_popover").click ->
          $('.popover').remove()

  get_popover_placement = (pop, dom_el) ->
    width = window.innerWidth
    width = width / 2
    left_pos = $(dom_el).offset().left
    return "left"  if width < left_pos
    "right"