class AddBattalionToMembers < ActiveRecord::Migration
  def change
    add_column :members, :battalion, :integer, default: 0
  end
end
