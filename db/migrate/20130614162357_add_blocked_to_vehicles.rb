class AddBlockedToVehicles < ActiveRecord::Migration
  def change
    add_column :vehicles, :blocked_to, :datetime, default: nil
  end
end
