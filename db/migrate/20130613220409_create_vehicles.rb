class CreateVehicles < ActiveRecord::Migration
  def change
    create_table :vehicles do |t|
      t.string :name
      t.string :level
      t.integer :member_id
      t.string :image

      t.timestamps
    end
  end
end
