class CreateMembers < ActiveRecord::Migration
  def change
    create_table :members do |t|
      t.string :name
      t.string :rank
      t.integer :account_id
      t.datetime :last_update

      t.timestamps
    end
  end
end
