NewTap::Application.routes.draw do
  devise_for :users
  mount RailsAdmin::Engine => '/admin', :as => 'rails_admin'

  match 'search_user' => 'search#search_user', as: :user_search
  match 'search_clan' => 'search#search_clan', as: :clan_search

  match 'search_user/:user' => 'search#user', as: :search_user
  match 'search_clan/:clan' => 'search#clan', as: :search_clan

  match 'clan_description/:clan_id' => 'search#clan_description', as: :clan_description

  match 'show_owners/:vehicle' => 'vehicles#show_owners', as: :show_owners

  match 'first_battalion' => 'members#first_battalion', as: :first_battalion
  match 'second_battalion' => 'members#second_battalion', as: :second_battalion
  match 'third_battalion' => 'members#third_battalion', as: :third_battalion
  match 'show_battalion_owners/:battalion/:vehicle_name' => 'vehicles#show_battalion_owners', as: :show_battalion_owners

  match 'check_block_status' => 'vehicles#check_block_status', as: :check_block_status

  resources :pages
  resources :members do
    collection do
      get :get_all
    end
    resources :vehicles do
      member do
        get :get_all
      end
    end
  end

  root to: 'home#index'
end
